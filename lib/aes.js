import Cryptr from 'cryptr';

export const
    encrypt = (value, password) => new Cryptr(password).encrypt(value),
    decrypt = (value, password) => new Cryptr(password).decrypt(value);