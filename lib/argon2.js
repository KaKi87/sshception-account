import argon2 from 'argon2';

export const
    hash = async password => await argon2.hash(password, { type: argon2.argon2id }),
    verify = argon2.verify;