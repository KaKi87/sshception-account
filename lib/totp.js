import twoFa from '2fa';

export const
    generate = name => new Promise((resolve, reject) => twoFa.generateKey((error, key) => {
        if(error) reject(error);
        else resolve({
            key,
            url: `otpauth://totp/${name}/?secret=${twoFa.base32Encode(key)}`
        });
    })),
    verify = (code, key) => twoFa.verifyTOTP(key, code);