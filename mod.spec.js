import { describe, before, it } from 'mocha';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { generate } from 'generate-password';

import totp from 'deathmoon-totp-generator';

import {
    login,
    register
} from './mod.js';

const
    generatePassword = () => generate({
        length: 18,
        numbers: true,
        symbols: true,
        lowercase: true,
        uppercase: true,
        strict: true
    });

chai.use(chaiAsPromised);

const
    password = generatePassword(),
    password2 = generatePassword();

describe('@sshception/account', () => {

    describe('login/register', () => {

        it('should not login to non-existing account', () => expect(login('username', 'password')).to.be.eventually.rejectedWith('WRONG_USERNAME'));

        it('should not register with insecure password', () => expect(register('username', 'password')).to.be.eventually.rejectedWith('PWNED_PASSWORD'));

        it('should register new account', () => expect(register('username', password)).to.be.fulfilled);

        it('should not register with already existing username', () => expect(register('username', password)).to.be.eventually.rejectedWith('ALREADY_EXISTS'));

        it('should not login with wrong password', () => expect(login('username', 'wrongpassword')).to.be.eventually.rejectedWith('WRONG_PASSWORD'));

        it('should login', () => expect(login('username', password)).to.be.fulfilled);

        describe('totp', () => {

            let _account, _getValidTotp;

            describe('enable totp', () => {

                let _account, _totp;

                before(async () => {
                    _account = await login('username', password);
                    _totp = await _account.enableTotp();
                    _getValidTotp = () => totp(_totp.url.split('=')[1]);
                });

                it('should not be enabled', () => expect(_account.isTotpEnabled()).to.be.false);

                it('should not be locked', () => expect(_account.isTotpLocked()).to.be.false);

                it('should return an url', () => expect(_totp.url).to.be.a('string'));

                it('should return a callback', () => expect(_totp.verify).to.be.a('function'));

                it('should not validate', () => expect(_totp.verify(1234)).to.be.false);

                it('should validate', () => expect(_totp.verify(_getValidTotp())).to.be.true);

                it('should not enable again', () => expect(_account.enableTotp()).to.be.eventually.rejectedWith('ALREADY_ENABLED'));

                it('should not be enabled', () => expect(_account.isTotpEnabled()).to.be.true);

            });

            describe('login totp', () => {

                before(async () => _account = await login('username', password));

                it('should be locked', () => expect(_account.isTotpLocked()).to.be.true);

                it('should not unlock', () => expect(_account.unlockTotp(1234)).to.be.false);

                it('should still be locked', () => expect(_account.isTotpLocked()).to.be.true);

                it('should validate', () => expect(_account.unlockTotp(_getValidTotp())).to.be.true);

                it('should be unlocked', () => expect(_account.isTotpLocked()).to.be.false);

            });

            describe('disable totp', () => {

                it('should not disable without totp code', () => expect(() => _account.disableTotp()).to.throw('TOTP_REQUIRED'));

                it('should not disable with wrong totp code', () => expect(() => _account.disableTotp(1234)).to.throw('WRONG_TOTP'));

                it('should disable', () => expect(_account.disableTotp(_getValidTotp())).to.be.true);

                it('should be disabled', () => expect(_account.isTotpEnabled()).to.be.false);

            });

        });

    });

    describe('hosts', () => {

        let _account;

        const host1v1 = {
            name: 'host',
            address: 'host',
            port: 22,
            username: 'username',
            password: 'password'
        };

        const host1v2 = {
            ...host1v1,
            address: 'host2',
            port: 1234
        };

        const host1v3 = {
            ...host1v2,
            name: 'renamedHost'
        };

        const host2 = {
            ...host1v1,
            name: 'host2'
        };

        before(async () => _account = await login('username', password));

        it('should return an empty array', () => expect(_account.getHosts()).to.deep.equal([]));

        it('should add host', () => expect(_account.addHost(host1v1)).to.be.true);

        it('should have added host', () => expect(_account.getHosts()).to.deep.include(host1v1));

        it('should add another host', () => expect(_account.addHost(host2)).to.be.true);

        it('should have added another host', () => expect(_account.getHosts()).to.deep.include(host2));

        it('should not add host with already existing name', () => expect(() => _account.addHost(host1v1)).to.throw('ALREADY_EXISTS'));

        it('should edit host', () => expect(_account.editHost(host1v1.name, host1v2)));

        it('should have edited host', () => expect(_account.getHosts()).to.deep.include(host1v2));

        it('should have edited host (2)', () => expect(_account.getHosts()).to.not.deep.include(host1v1));

        it('should rename host', () => expect(_account.editHost(host1v2.name, host1v3)).to.be.true);

        it('should have renamed host', () => expect(_account.getHosts()).to.deep.include(host1v3));

        it('should have renamed host (2)', () => expect(_account.getHosts()).to.not.deep.include(host1v2));

        it('should delete host', () => expect(_account.removeHost(host1v3.name)).to.be.true);

        it('should have deleted host', () => expect(_account.getHosts()).to.not.deep.include(host1v3));

        it('should not delete already deleted host', () => expect(() => _account.removeHost(host1v3.name)).to.throw('WRONG_NAME'));

        describe('alphabetical order', () => {

            before('remove secondary example host & add hosts in random order', () => {

                _account.removeHost(host2.name);

                _account.addHost({ ...host1v1, name: 'Charlie' });

                _account.addHost({ ...host1v1, name: 'Tango' });

                _account.addHost({ ...host1v1, name: 'Alpha' });

                _account.addHost({ ...host1v1, name: 'Romeo' });

            });

            it('should store hosts in alphabetical order', () => expect(_account.getHosts().map(host => host.name)).to.deep.equal(['Alpha', 'Charlie', 'Romeo', 'Tango']));

        });

    });

    describe('change username & password', () => {

        it('should change username', async () => expect((await login('username', password)).setUsername(password, 'username2')).to.be.fulfilled);

        it('should have changed username', () => expect(login('username2', password)).to.be.fulfilled);

        it('should change password', async () => expect((await login('username2', password)).setPassword(password, password2)).to.be.fulfilled);

        it('should have changed password', () => expect(login('username2', password2)).to.be.fulfilled);

        after('reset username & password', async () => {
            const _account = await login('username2', password2);
            await _account.setUsername(password2, 'username');
            await _account.setPassword(password2, password);
        });

    });

    describe('unregister', () => {

        let _account;

        before(async () => _account = await login('username', password));

        it('should unregister', () => expect(_account.unregister(password)).to.be.fulfilled);

        it('should be unregistered', () => expect(login('username', password)).to.eventually.be.rejectedWith('WRONG_USERNAME'));

    });

});