import {
    existsSync,
    renameSync,
    readFileSync,
    writeFileSync,
    unlinkSync
} from 'node:fs';
import path from 'node:path';

import Joi from 'joi';
import { pwnedPassword } from 'hibp';

import {
    verify as verifyArgon2,
    hash as hashArgon2
} from './lib/argon2.js';
import {
    decrypt,
    encrypt
} from './lib/aes.js';
import {
    verify as verifyTotp,
    generate as generateTotp
} from './lib/totp.js';

const cwd = process.cwd();

const account = username => {
    const
        userPasswordFile = (_username = username) => path.join(cwd, `./data/${_username}.pwd`),
        userDataFile = (_username = username) => path.join(cwd, `./data/${_username}.aes`),
        userKeyFile = (_username = username) => path.join(cwd, `./data/${_username}.key`);
    const _account = {
        isExisting: () => existsSync(userPasswordFile()),
        createOrUpdateUsername: newUsername => {
            if(existsSync(userPasswordFile()))
                renameSync(userPasswordFile(), userPasswordFile(newUsername));
            if(existsSync(userDataFile()))
                renameSync(userDataFile(), userDataFile(newUsername));
            if(existsSync(userKeyFile()))
                renameSync(userKeyFile(), userKeyFile(newUsername));
            return account(newUsername);
        },
        verifyPassword: async password => await verifyArgon2(readFileSync(userPasswordFile(), 'utf8'), password),
        createOrUpdatePassword: async password => writeFileSync(userPasswordFile(), await hashArgon2(password), 'utf8'),
        isTotpEnabled: () => existsSync(userKeyFile()),
        getTotpKey: password => decrypt(readFileSync(userKeyFile(), 'utf8'), password),
        verifyTotp: (password, code) => verifyTotp(code, _account.getTotpKey(password)),
        setTotpKey: (password, key) => writeFileSync(userKeyFile(), encrypt(key, password)),
        disableTotp: () => unlinkSync(userKeyFile()),
        unregister: () => {
            unlinkSync(userPasswordFile());
            unlinkSync(userDataFile());
            if(_account.isTotpEnabled())
                unlinkSync(userKeyFile());
        },
        getData: password => JSON.parse(decrypt(readFileSync(userDataFile(), 'utf8'), password)),
        setData: (password, data) => writeFileSync(userDataFile(), encrypt(JSON.stringify(data), password), 'utf8')
    };
    return _account;
};

export const
    isHostNameValid = name => !Joi.string().validate(name).error,
    isHostAddressValid = address => !Joi.string().hostname().validate(address).error,
    isHostPortValid = port => !Joi.number().integer().validate(port).error,
    isHostUsernameValid = username => !Joi.string().validate(username).error,
    isHostPasswordValid = password => !Joi.string().validate(password).error;

const Account = function(username, password){
    let _account = account(username);
    let _isTotpLocked = _account.isTotpEnabled();
    const _requirePassword = async password => {
        if(_account.isExisting() && !await _account.verifyPassword(password))
            throw new Error('WRONG_PASSWORD');
    };
    const _requireTotp = (code, verify) => {
        if(!_account.isTotpEnabled()) return;
        if(_isTotpLocked || (verify && !code))
            throw new Error('TOTP_REQUIRED');
        if(verify && !_account.verifyTotp(password, code))
            throw new Error('WRONG_TOTP');
    };
    const _getData = () => _account.getData(password);
    const _setData = data => _account.setData(password, data);
    const _hostExists = name => !!_getData().hosts.find(host => host.name === name);
    const _setHost = host => {
        const data = _getData();
        const _host = data.hosts.find(_host => _host.name === host.name);
        if(!isHostNameValid(host.name))
            throw new Error('INVALID_NAME');
        if(!isHostAddressValid(host.address))
            throw new Error('INVALID_ADDRESS');
        if(!isHostPortValid(host.port))
            throw new Error('INVALID_PORT');
        if(!isHostUsernameValid(host.username))
            throw new Error('INVALID_USER');
        if(!isHostPasswordValid(host.password))
            throw new Error('INVALID_PASSWORD');
        if(_host)
            Object.assign(_host, host);
        else
            data.hosts.push(host);
        data.hosts.sort((host1, host2) => host1.name.toLowerCase() < host2.name.toLowerCase() ? -1 : 1);
        _setData(data);
    };
    this.isTotpEnabled = () => _account.isTotpEnabled();
    this.enableTotp = async () => {
        if(_account.isTotpEnabled())
            throw new Error('ALREADY_ENABLED');
        const { url, key } = await generateTotp('sshception');
        return {
            url,
            verify: code => {
                const isValid = verifyTotp(code, key);
                if(isValid) _account.setTotpKey(password, key);
                return isValid;
            }
        }
    };
    this.disableTotp = code => {
        _requireTotp(code, true);
        _account.disableTotp();
        return true;
    };
    this.isTotpLocked = () => _isTotpLocked;
    this.unlockTotp = code => {
        const isValid = _account.verifyTotp(password, code);
        if(isValid) _isTotpLocked = false;
        return isValid;
    };
    this.unregister = async (currentPassword, code) => {
        await _requirePassword(currentPassword);
        _requireTotp(code, true);
        _account.unregister();
        return true;
    };
    this.getUsername = () => username;
    this.setUsername = async (currentPassword, newUsername, code) => {
        if(!/^[a-zA-Z0-9]+$/.test(username))
            throw new Error('INVALID_USERNAME');
        if(account(newUsername).isExisting())
            throw new Error('ALREADY_EXISTS');
        await _requirePassword(currentPassword);
        _requireTotp(code, true);
        _account = _account.createOrUpdateUsername(newUsername);
        username = newUsername;
        return true;
    };
    this.setPassword = async (currentPassword, newPassword, code) => {
        if(await pwnedPassword(newPassword))
            throw new Error('PWNED_PASSWORD');
        await _requirePassword(currentPassword);
        _requireTotp(code, true);
        await _account.createOrUpdatePassword(newPassword);
        try { _account.setData(newPassword, _getData()); } catch(_){}
        try { _account.setTotpKey(newPassword, _account.getTotpKey(password)) } catch(_){}
        password = newPassword;
        return true;
    };
    this.getHosts = () => {
        _requireTotp();
        return _getData().hosts;
    };
    this.addHost = host => {
        _requireTotp();
        if(_hostExists(host.name))
            throw new Error('ALREADY_EXISTS');
        _setHost(host);
        return true;
    };
    this.removeHost = name => {
        _requireTotp();
        const data = _getData();
        if(!_hostExists(name))
            throw new Error('WRONG_NAME');
        data.hosts.splice(data.hosts.indexOf(data.hosts.find(_host => _host.name === name)), 1);
        _setData(data);
        return true;
    };
    this.editHost = (currentName, host) => {
        _requireTotp();
        _setHost(host);
        if(currentName !== host.name)
            this.removeHost(currentName);
        return true;
    };
    this._initialize = async () => {
        await this.setUsername(undefined, username);
        await this.setPassword(undefined, password);
        _setData({
            hosts: []
        });
        return true;
    };
    this._login = async () => {
        if(!_account.isExisting())
            throw new Error('WRONG_USERNAME');
        await _requirePassword(password);
    };
};

export const
    preregister = async (username, password) => {
        if(!/^[a-zA-Z0-9]+$/.test(username))
            throw new Error('INVALID_USERNAME');
        if(account(username).isExisting())
            throw new Error('ALREADY_EXISTS');
        if(await pwnedPassword(password))
            throw new Error('PWNED_PASSWORD');
    },
    register = async (username, password) => {
        const account = new Account(username, password);
        await account._initialize();
        return account;
    },
    login = async (username, password) => {
        const account = new Account(username, password);
        await account._login();
        return account;
    };